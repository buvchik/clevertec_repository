package com.example.clevertec_project

import android.annotation.SuppressLint
import android.app.Application
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.ViewCompat
import androidx.core.view.marginBottom
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.FragmentTransaction
import androidx.transition.TransitionInflater
import kotlinx.android.synthetic.main.fragment_item.*
import kotlinx.android.synthetic.main.fragment_item.view.*
import android.view.Gravity
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT


class SecondFragment : Fragment() {
    lateinit var fr_view: View
companion object{
    val ID_TOOLBAR = 1000
}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSharedElementEnterTransition(TransitionInflater.from(getContext()).inflateTransition(android.R.transition.move));
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fr_view = inflater.inflate(R.layout.fragment_item, container, false)
        return fr_view
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                // todo: goto back activity from here
//                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }

    @SuppressLint("ResourceType")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            var toolbar = context?.let { androidx.appcompat.widget.Toolbar(it) }
            toolbar?.popupTheme = android.R.style.TextAppearance_Theme
            toolbar?.setBackgroundColor(Color.BLUE)
            toolbar?.visibility = View.VISIBLE
            toolbar?.setTitle("")
            toolbar?.id = ID_TOOLBAR
            toolbar?.layoutParams = ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                WRAP_CONTENT
            )
            (activity as AppCompatActivity).setSupportActionBar(toolbar)

            var imgBack = Button(context)
            imgBack.setText("Back")
            imgBack.setBackgroundColor(Color.TRANSPARENT)
            val l = Toolbar.LayoutParams(WRAP_CONTENT, 150)
            l.gravity = Gravity.START
            imgBack.setLayoutParams(l)
            toolbar?.addView(imgBack)
            imgBack.setOnClickListener {
                getActivity()?.onBackPressed()
            }
            var imgClose = Button(context)
            imgClose.setText("Close")
            imgClose.setBackgroundColor(Color.TRANSPARENT)
            l.gravity = Gravity.END
            imgClose.setLayoutParams(l)
            toolbar?.addView(imgClose)
            imgClose.setOnClickListener {
                getActivity()?.finish()
            }
            fr_view.cLayout.addView(toolbar, 0)
        }


        val bundle = this.arguments
        if (bundle != null) {
            val title = bundle.getString("title", "")
            val description = bundle.getString("description", "")
            val img = bundle.getInt("img", 0)
            val imgw = bundle.getInt("imgW", 0)
            val imgh = bundle.getInt("imgH", 0)


            image.setImageResource(img)
            image.minimumWidth = imgw * 2
            image.minimumHeight = imgh * 2


            tv_title.setText(title)
            tv_title.setTextAppearance(context,R.style.for_tv_title)

            tv_description.setText(description)
            tv_description.setTextAppearance(context,R.style.for_tv_description)

            image.updateLayoutParams<ConstraintLayout.LayoutParams> {
                endToEnd = R.id.cLayout
            }
            tv_title.updateLayoutParams<ConstraintLayout.LayoutParams> {
                startToStart = R.id.image
                endToEnd = R.id.image
                topToBottom = R.id.image //почему-то не работает
            }
            tv_description.updateLayoutParams<ConstraintLayout.LayoutParams> {
                startToStart = R.id.image
                endToEnd = R.id.image
            }

            val constraintSet = ConstraintSet()
            constraintSet.clone(cLayout)
            constraintSet.connect(R.id.image,ConstraintSet.TOP,ID_TOOLBAR,ConstraintSet.BOTTOM)
            constraintSet.connect(R.id.tv_title,ConstraintSet.TOP,R.id.image,ConstraintSet.BOTTOM)
            constraintSet.applyTo(cLayout)
        }

        val btn_Shared_Anim = Button(context)
        btn_Shared_Anim.text = getString(R.string.run_animation)
        fr_view.cLayout.addView(btn_Shared_Anim)
        btn_Shared_Anim.updateLayoutParams<ConstraintLayout.LayoutParams>  {
            bottomToBottom  = R.id.tv_description
            endToEnd = R.id.cLayout
        }
        btn_Shared_Anim.setOnClickListener {
            val fragment: Fragment = SecondFragment()

            val args = Bundle()
            args.putString("title", tv_title.text.toString())
            args.putString("description", tv_description.text.toString())
            args.putInt("img", R.drawable.smile)

            args.putInt("imgW", image.width*3)
            args.putInt("imgH", image.width*3)
            fragment.arguments = args

            ViewCompat.getTransitionName(image)?.let {
                getActivity()?.getSupportFragmentManager()?.beginTransaction()?.addSharedElement(image, it)?.
                addToBackStack("second")?.
                replace(R.id.frgmCont, fragment)?.
                commit()
            }
        }


    }

}