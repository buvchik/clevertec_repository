package com.example.clevertec_project

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView

class FirstFragment : Fragment() {
    lateinit var fr_view:View
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        fr_view = inflater.inflate(R.layout.fragment_item_list, container, false)
        return fr_view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val rv =view.findViewById<RecyclerView>(R.id.rv_items)
        rv.setHasFixedSize(true)
        val list_item :MutableList<Int> = mutableListOf()
        for (i in 0..1000) list_item.add(i)
        rv.adapter = RvAdapter(list_item,this)

    }
    class RvAdapter(list: List<Int>, firstFragment: FirstFragment): RecyclerView.Adapter<RvAdapter.ViewHolder>() {
        var list_item : List<Int>
        var firstFragment:Fragment
        init {
            this.list_item = list
            this.firstFragment = firstFragment
        }
        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
         val img: ImageView
         val tv_title:TextView
         val tv_description:TextView
          init {
              img = itemView.findViewById(R.id.image)
              tv_title = itemView.findViewById(R.id.tv_title)
              tv_description= itemView.findViewById(R.id.tv_description)
          }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.fragment_item,parent,false)
            return ViewHolder(v)
        }
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.img.setImageResource(R.drawable.smile)
            holder.tv_title.setText("Title $position")
            holder.tv_description.setText("Description $position")
            holder.itemView.setOnClickListener {
                    val fragment: Fragment = SecondFragment()

                val args = Bundle()
                    args.putString("title", "Title "+ position)
                    args.putString("description", "Description "+ position)
                    args.putInt("img", R.drawable.smile)

                    args.putInt("imgW", holder.img.width)
                    args.putInt("imgH", holder.img.height)
                    fragment.arguments = args

                    val transaction: FragmentTransaction? = firstFragment.getActivity()?.getSupportFragmentManager()?.beginTransaction()
                    transaction?.replace(R.id.frgmCont, fragment)
                    transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    transaction?.addToBackStack("first")
                    transaction?.commit()
                }
            }

        override fun getItemCount(): Int = list_item.size

    }

}